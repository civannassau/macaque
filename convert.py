#!/usr/bin/env python3

"""
Script that gets a list with all the input files.
The input files are SAM files. They are converted
to BAM files using the tool samtools.
"""

__author__ = 'Carlijn Ruijter'


#IMPORT
import sys
import os
import subprocess


class ConvertToBam():
    """
    Class that converts input SAM files to
    BAM files.
    """

    def __init__(self, input_path, output_path):
        """
        Set the attributes for the class.
        :param input_path: path to the folder with the input sam files
        :param output_path: path to the folder for the output files
        """

        self.input_path = input_path
        self.output_path = output_path


    def get_files(self):
        """
        Get a list with the input files for the tool.
        :return: list with the names of the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)

        # return the files
        return files_list


    def run_command(self, input_file, number_of_threads):
        """
        Make the command to convert the SAM files to BAM files.
        :param input_file: an input SAM file that will be converted to a BAM file
        """

        # the command to convert a SAM file to a BAM file
        convert_command = 'samtools view -Sb -o {}/{}.bam -@ {} {}/{}'\
                            .format(self.output_path,
                                    input_file.replace('.sam', ''),
                                    number_of_threads, self.input_path, input_file)

        # execute the command
        subprocess.check_call(convert_command, shell=True)

def main(args):
    """
    Make a convert object for the Dutch and Brazilian cohort.
    """

    # object for the NL cohort with the path for the input and output folder
    convert_NL = ConvertToBam('/data/storage/students/2018-2019/Thema06/bowtie_macaque_NL',
                              '/data/storage/students/2018-2019/Thema06/groep3/convert_to_bam_NL')

    # get the input files
    input_files_NL = convert_NL.get_files()

    # loop through the input files and run the command per file
    for file in input_files_NL:
        convert_NL.run_command(file, 8)


    # object for the BZ cohort with the path for the input and output folder
    convert_BZ = ConvertToBam('/data/storage/students/2018-2019/Thema06/groep3/bowtie_BZ',
                              '/data/storage/students/2018-2019/Thema06/groep3/convert_to_bam_BZ')

    # get the input files
    input_files_BZ = convert_BZ.get_files()

    # loop through the input files and run the command per file
    for file in input_files_BZ:
        convert_BZ.run_command(file, 8)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
