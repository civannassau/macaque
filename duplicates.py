#!/usr/bin/env python3

"""
Mark the duplicates that are present in the data
by using the MarkDuplicates tool from picard.
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import sys
import os
import subprocess


class Duplicates():
    """
    Class that gets the files from a given folder
    and a method with an command for executing the MarkDuplicates tool
    on every input file.
    """

    def __init__(self, picard_file, input_path, output_path):
        """
        Method that gets the attributes that every object will use.
        :param picard_file: path to the picard file used for the tools
        :param input_path: path to the input files
        :param output_path: path for the output files
        """
        self.picard_file = picard_file
        self.input_path = input_path
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files
        :return: list with the files
        """

        # use os to get all the files from the input folder
        files = os.listdir(self.input_path)

        # return the files
        return files

    def run_command(self, input_file, number_of_threads):
        """
        Make the command for the MarkDuplicates tool.
        :param input_file: an input sam or bam file
        """
        # the command for MarkDuplicates
        duplicates_command = 'java -XX:ParallelGCThreads={} -jar {}'\
                             'MarkDuplicates I={}/{} O={}/dupl_{} '\
                             'M={}/dupl_{} CREATE_INDEX=true'\
                             .format(number_of_threads,
                                     self.picard_file, self.input_path, input_file,
                                     self.output_path, input_file.replace('fixmate_', ''),
                                     self.output_path,
                                     input_file.replace('.bam', '.txt').replace('fixmate_', ''))

        # execute the command and wait until it completes before running the next
        subprocess.check_call(duplicates_command, shell=True)


def main(args):
    """
    Make the objects for executing the MarkDuplicates tool
    on the Dutch and Brazilian cohorts.
    """

    # object for the NL cohort
    duplicates_NL = Duplicates('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                               '/data/storage/students/2018-2019/Thema06/groep3/fixmate_NL',
                               '/data/storage/students/2018-2019/Thema06/groep3/markduplicates_NL')

    # get the input files
    input_files_NL = duplicates_NL.get_files()

    # loop through the input files and run the command per file
    for file in input_files_NL:
        duplicates_NL.run_command(file, 40)

    # object for the BZ cohort
    duplicates_BZ = Duplicates('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                               '/data/storage/students/2018-2019/Thema06/groep3/fixmate_BZ',
                               '/data/storage/students/2018-2019/Thema06/groep3/markduplicates_BZ')

    # get the input files
    input_files_BZ = duplicates_BZ.get_files()

    # loop through the input files and run the command per file
    for file in input_files_BZ:
        duplicates_BZ.run_command(file, 40)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
