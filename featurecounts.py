#!/usr/bin/env python3

"""
A script that gets all the input files together
and executes the featureCounts tool on them all
to create one output text file with a count matrix.
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import sys
import os
import subprocess


class FeatureCounts():
    """
    Class that gets the files from a given folder and
    uses all the files together to make one count matrix
    with the featureCounts tool.
    """

    def __init__(self, input_path, path_to_gtf, output_path):
        """
        Method that gets the attributes that every object will use.
        :param input_path: path to the input files
        :param path_to_gtf: path to the gtf file that need to be used for featureCounts
        :param output_path: path for the output file
        """

        self.input_path = input_path
        self.path_to_gtf = path_to_gtf
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files
        :return: list with the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)
        files = []
        # get the files plus the full path name to the file
        for file in files_list:
            file = self.input_path + '/' + file
            files.append(file)
        full_path = []
        for root, dirs, files in os.walk(self.input_path):
            full_path = [root + '/' + file for file in files]
        return full_path

    def run_command(self, input_files):
        """
        Make the command for the featureCounts tool.
        :param input_files: all the input files that will generate a count matrix
        """
        # the command for featureCounts
        feat_counts_command = 'featureCounts -a {} -o {}/featurecounts.txt {}'\
                              .format(self.path_to_gtf, self.output_path, input_files)

        # execute the command and wait until it completes before running the next
        subprocess.check_call(feat_counts_command, shell=True)


def main(args):
    """

    Make an object for the featurecounts command and getting all the files together.
    """
    # object for the NL cohort
    count_NL = FeatureCounts('/data/storage/students/2018-2019/Thema06/groep3/sortsamtools2_NL',
                             '/data/storage/students/2018-2019/Thema06/groep3/'\
                             'annotationfile/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf',
                             '/data/storage/students/2018-2019/Thema06/groep3/fc_NL')

    # get the input files for the NL cohort
    input_files_NL = count_NL.get_files()


    # object for the BZ cohort
    count_BZ = FeatureCounts('/data/storage/students/2018-2019/Thema06/groep3/sortsamtools2_BZ',
                             '/data/storage/students/2018-2019/Thema06/groep3/'\
                             'annotationfile/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf',
                             '/data/storage/students/2018-2019/Thema06/groep3/fc_BZ')

    # get the input files for the BZ cohort
    input_files_BZ = count_BZ.get_files()

    # make 1 string of the two lists
    input_files = ' '.join(input_files_NL) + ' ' + ' '.join(input_files_BZ)

    # execute the command with all the files
    count_NL.run_command(input_files)

    return 0


#

if __name__ == '__main__':
    sys.exit(main(sys.argv))
