#!/usr/bin/env python3

"""
Align the data with the genome of the macaque
by using the bowtie2 tool.
"""

__author__ = 'Carlijn Ruijter', 'Jildou Haaijer'

# IMPORT
import os
import re
import subprocess
import sys


class Bowtie2():
    """
    Class that gets the input fastq files that need to be
    aligned to the genome of the macaque and align them
    by using the bowtie2 tool.
    """

    def __init__(self, input_single_end, input_paired_end, index_path, output_path):
        """
        Class that makes the variables used in every made object
        :param input_single_end: path to the fastq files that are single end
        :param input_paired_end: path to the fastq files that are paired end
        :param index_path: path to the used index from the genome
        :param output_path: path to the output folder
        """
        self.input_single_end = input_single_end
        self.input_paired_end = input_paired_end
        self.index_path = index_path
        self.output_path = output_path

    def get_files(self):
        """
        Get the correct fastq files for a paired end
        and a single end cohort.
        :return: a list with the fastq files
        """
        # List with all the fastq files from the NL cohort, single end
        single_end_cohort = (os.listdir(self.input_single_end))

        # List with all the fastq files from the BZ cohort, paired end
        paired_end_files = (os.listdir(self.input_paired_end))
        cohort_R1 = []
        cohort_R2 = []
        for file in paired_end_files:
            if re.search('R1', file):
                cohort_R1.append(file)
            else:
                cohort_R2.append(file)

        # sort the lists, so that they will be in the same order
        cohort_R1 = sorted(cohort_R1)
        cohort_R2 = sorted(cohort_R2)

        # zip the lists together
        paired_end_cohort = list(zip(sorted(cohort_R1), sorted(cohort_R2)))

        # return the lists with the fastq files
        return single_end_cohort, paired_end_cohort

    def run_command_single_end(self, file, output_folder, number_of_cores):
        """
        Run the bowtie2 program for single end files
        :param file: input file for the tool
        :param output_folder: name of the output folder
        :param number_of_cores: number of cores used for the tool
        """

        # command line for bowtie2 for single end cohort
        bowtie_command = 'bowtie2 -x {} -U {}/{} -p {} -S {}/{}/bowtie_{}.sam' \
                            .format(self.index_path,
                                    self.input_single_end, file,
                                    number_of_cores,
                                    self.output_path, output_folder,
                                    re.sub('.fastq.gz|.fastq', '', file))

        # execute the command
        subprocess.check_call(bowtie_command, shell=True)

    def run_command_paired_end(self, file_1, file_2, output_folder, number_of_cores):
        """
        Run the bowtie2 program for paired end files
        """
        bowtie_command = 'bowtie2 -x {} -1 {}/{} -2 {}/{} -p {} -S {}/{}/bowtie_{}.sam' \
                            .format(self.index_path,
                                    self.input_paired_end, file_1,
                                    self.input_paired_end, file_2,
                                    number_of_cores,
                                    self.output_path, output_folder,
                                    re.sub('.fastq.gz|.fastq', '', file_1))

        # execute the command
        subprocess.check_call(bowtie_command, shell=True)


def main(args):
    """
    Make an object that can run bowtie2 for a single end
    dataset and a paired end dataset.
    """

    bowtie2 = Bowtie2('/commons/Themas/Thema06/Themaopdracht/macaque_story/'\
                      'RNA_Microglia/MacaqueNL/fastqFiles',
                      '/commons/Themas/Thema06/Themaopdracht/macaque_story/'\
                      'RNA_Microglia/MacaqueBrazil/fastqFiles',
                      '/data/storage/students/2018-2019/Thema06/macaque/MacaM_Rhesus_Genome_v7',
                      '/data/storage/students/2018-2019/Thema06/groep3')

    # get the input fastq files
    fastq_files_NL, fastq_files_BZ = bowtie2.get_files()

    # loop through the files and execute the bowtie2 tool for the file
    for file in fastq_files_NL:
        bowtie2.run_command_single_end(file, 'bowtie_NL', 20)

    # loop through the files and get the pairs, run the command per pair
    for first_half, second_half in fastq_files_BZ:
        bowtie2.run_command_paired_end(first_half, second_half, 'bowtie_BZ', 20)


    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
