#!/usr/bin/env python3

"""
A pipeline that contains all the steps
to process a fastq file to a count matrix
made with featurecounts.
"""

__author__ = 'Jildou Haaijer', 'Carlijn Ruijter'

#IMPORT
import sys
# import the classes from the modules
from fastqc import Fastqc
from bowtie2 import Bowtie2
from convert import ConvertToBam
from sortsam import SortSam
from readgroups import ReadGroups
from fixmate import FixMate
from duplicates import Duplicates
from samtools import FinalSort
from featurecounts import FeatureCounts


def fastqc(input_path, output_path):
    """ Make an object for the FastQC tool. """

    # make an object from the class
    fastqc = Fastqc(input_path, output_path)

    # get the input files
    fastq_files = fastqc.get_files()

    # loop through the input files and run the command per file
    for file in fastq_files:
        fastqc.run_command(file)

def bowtie2(index_path, input_path, input_path2, output_path, number_of_cores):
    """ Make an object for the Bowtie2 tool. """

    # make an object from the class
    bowtie2 = Bowtie2(index_path, input_path, input_path2, output_path)

    # get the input files
    fastq_files_NL, fastq_files_BZ = bowtie2.get_files()

    # loop through the files and execute the bowtie2 tool for the single end files
    for file in fastq_files_NL:
        bowtie2.run_command_single_end(file, 'bowtie_NL', number_of_cores)

    # loop through the files and get the pairs, run the command per pair
    for first_half, second_half in fastq_files_BZ:
        bowtie2.run_command_paired_end(first_half, second_half, 'bowtie_BZ', number_of_cores)

    # return the output path, that will be used in the next tool
    return output_path

def convert_to_bam(input_path, output_path, number_of_threads):
    """
    Make a convert object.
    """

    # object with the path for the input and output folder
    convert = ConvertToBam(input_path, output_path)

    # get the input files
    input_files = convert.get_files()

    # loop through the input files and run the command per file
    for file in input_files:
        convert.run_command(file, number_of_threads)

    # return the output path, that will be used in the next tool
    return output_path

def sortsam(picard_path, input_path, output_path):
    """
    Make a sortsam object using the SortSam class.
    """

    # make an object from the class
    sortsam = SortSam(picard_path, input_path, output_path)

    # get the input files
    input_files = sortsam.get_files()

    # loop through the input files and run the command per file
    for file in input_files:
        sortsam.run_command(file)

    # return the output path, that will be used in the next tool
    return output_path

def readgroups(picard_path, input_path, output_path):
    """
    Make a fixmate object using the FixMate class.
    """

    # make an object from the class
    readgroups = ReadGroups(picard_path, input_path, output_path)

    # get the input files
    input_files = readgroups.get_files()

    # loop through the input files and run the command per file
    for file in input_files:
        readgroups.run_command(file)

    # return the output path, that will be used in the next tool
    return output_path

def fixmate(picard_path, input_path, output_path, number_of_threads):
    """
    Make a fixmate object using the FixMate class.
    """
    # make an object from the class
    fixmate = FixMate(picard_path, input_path, output_path)

    # get the input files
    input_files = fixmate.get_files()

    # loop through the input files and run the command per file
    for file in input_files:
        fixmate.run_command(file, number_of_threads)

    # return the output path, that will be used in the next tool
    return output_path

def duplicates(picard_path, input_path, output_path, number_of_threads):
    """
    Make a duplicates object using the Duplicates class.
    """
    # make an object from the class
    duplicates = Duplicates(picard_path, input_path, output_path)

    # get the input files
    input_files = duplicates.get_files()

    # loop through the input files and run the command per file
    for file in input_files:
        duplicates.run_command(file, number_of_threads)

    # return the output path, that will be used in the next tool
    return output_path

def finalsort(input_path, output_path, number_of_threads):
    """
    Make a finalsort object using the FinalSort class.
    """
    # make an object from the class
    finalsort = FinalSort(input_path, output_path)

    # get the input files
    input_files = finalsort.get_files()

    # loop through the input files and run the command per file
    for file in input_files:
        finalsort.run_command(file, number_of_threads)

    # return the output path, that will be used in the next tool
    return output_path

def featurecounts(input_path, annotation_file, output_path):
    """
    Make a featurecounts object using the FeatureCounts class.
    """
    # make an object from the class
    featurecounts = FeatureCounts(input_path, annotation_file, output_path)

    # get the input files
    input_files = featurecounts.get_files()
    input_files = ' '.join(input_files)
    return input_files, featurecounts

def main(args):
    """
    Get all the output_paths to run the next tool
    with the output files from the previous tool.
    """

    # path to the picard.jar file, used for the tools from picard
    picard_path = '/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar'

    ## Run pipeline for the Dutch cohort
    # execute the fastqc tool on the fastq files
    fastqc('/commons/Themas/Thema06/Themaopdracht/'\
           'macaque_story/RNA_Microglia/MacaqueNL/fastqFiles',
           '/data/storage/students/2018-2019/Thema06/groep3/fastqc_NL')
    # execute the bowtie2 tool and get the output path as input path for the next tool
    input_convert = bowtie2('/commons/Themas/Thema06/Themaopdracht/macaque_story/'\
                            'RNA_Microglia/MacaqueNL/fastqFiles',
                            '/commons/Themas/Thema06/Themaopdracht/macaque_story/'\
                            'RNA_Microglia/MacaqueBrazil/fastqFiles',
                            '/data/storage/students/2018-2019/Thema06/'\
                            'macaque/MacaM_Rhesus_Genome_v7',
                            '/data/storage/students/2018-2019/Thema06/groep3', 20)
    # execute the convert tool and get the output path as input path for the next tool
    input_sortsam_nl = convert_to_bam(input_convert + 'bowtie_NL',
                                      '/data/storage/students/2018-2019/'\
                                      'Thema06/groep3/convert_to_bam_NL', 20)
    # execute the sortsam tool and get the output path as input path for the next tool
    input_readgroups_nl = sortsam(picard_path, input_sortsam_nl,
                                  '/data/storage/students/2018-2019/Thema06/groep3/sortsam_NL')
    # execute the readgroups tool and get the output path as input path for the next tool
    input_fixmate_nl = readgroups(picard_path, input_readgroups_nl,
                                  '/data/storage/students/2018-2019/Thema06/groep3/readgroups_NL')
    # execute the fixmate tool and get the output path as input path for the next tool
    input_duplicates_nl = fixmate(picard_path, input_fixmate_nl,
                                  '/data/storage/students/2018-2019/Thema06/groep3/fixmate_NL', 20)
    # execute the duplicates tool and get the output path as input path for the next tool
    input_finalsort_nl = duplicates(picard_path, input_duplicates_nl,
                                    '/data/storage/students/2018-2019/Thema06/'\
                                    'groep3/markduplicates_NL', 20)
    # execute the finalsort tool and get the output path as input path for the next tool
    input_featurecounts_nl = finalsort(input_finalsort_nl,
                                       '/data/storage/students/2018-2019/Thema06/'\
                                       'groep3/sortsamtools2_NL', 20)

    ## Run pipeline for the Brazilian cohort
    # execute the fastqc tool on the fastq files
    fastqc('/commons/Themas/Thema06/Themaopdracht/'\
           'macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles',
           '/data/storage/students/2018-2019/Thema06/groep3/fastqc_BZ')
    # bowtie2 is executed above

    # execute the convert tool and get the output path as input path for the next tool
    input_sortsam_bz = convert_to_bam(input_convert + 'bowtie_BZ',
                                      '/data/storage/students/2018-2019/Thema06/'\
                                      'groep3/convert_to_bam_BZ', 20)
    # execute the sortsam tool and get the output path as input path for the next tool
    input_readgroups_bz = sortsam(picard_path, input_sortsam_bz,
                                  '/data/storage/students/2018-2019/Thema06/groep3/sortsam_BZ')
    # execute the readgroups tool and get the output path as input path for the next tool
    input_fixmate_bz = readgroups(picard_path, input_readgroups_bz,
                                  '/data/storage/students/2018-2019/Thema06/groep3/readgroups_BZ')
    # execute the fixmate tool and get the output path as input path for the next tool
    input_duplicates_bz = fixmate(picard_path, input_fixmate_bz,
                                  '/data/storage/students/2018-2019/Thema06/groep3/fixmate_BZ', 20)
    # execute the duplicates tool and get the output path as input path for the next tool
    input_finalsort_bz = duplicates(picard_path, input_duplicates_bz,
                                    '/data/storage/students/2018-2019/Thema06/'\
                                    'groep3/markduplicates_BZ', 20)
    # execute the finalsort tool and get the output path as input path for the next tool
    input_featurecounts_bz = finalsort(input_finalsort_bz,
                                       '/data/storage/students/2018-2019/Thema06/'\
                                       'groep3/sortsamtools2_BZ', 20)
    # get all the files from the Dutch cohort together in one string
    input_files_nl, feat = featurecounts(input_featurecounts_nl,
                                         '/data/storage/students/2018-2019/Thema06/groep3/'\
                                         'annotationfile/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf',
                                         '/data/storage/students/2018-2019/Thema06/groep3/featurecounts')
    # get all the files from the Brazilian cohort together in one string
    input_files_bz, feat = featurecounts(input_featurecounts_bz,
                                         '/data/storage/students/2018-2019/Thema06/groep3/'\
                                         'annotationfile/MacaM_Rhesus_Genome_Annotation_v7.6.8.gtf',
                                         '/data/storage/students/2018-2019/Thema06/groep3/featurecounts')
    # run the command for featurecounts on the ducth and brazilian files
    feat.run_command(input_files_nl + ' ' + input_files_bz)


if __name__ == '__main__':
    sys.exit(main(sys.argv))
