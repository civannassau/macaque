#!/usr/bin/env python3

"""
Check the quality of fastq files by
using the FASTQC program.
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import os
import sys
import subprocess

class Fastqc():
    """
    Class that gets the correct fastq files
    and checks the quality of the reads in the file
    by using the fastqc tool.
    """

    def __init__(self, input_path, output_path):
        """
        Method that gets the variables, used when the object is made
        :param input_path: the path to the input fastq files
        :param output_path: the path to the folder for the output files
        """
        self.input_path = input_path
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files for the tool.
        :return: list with the names of the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)

        # return the files
        return files_list

    def run_command(self, input_file):
        """
        Run the fastqc program on the command line
        with all the fastqc files
        :param input_file: input file for the tool
        """
        # make command line for the fastqc tool
        fastqc_command = 'fastqc -o {} {}/{}'\
                            .format(self.output_path,
                                    self.input_path, input_file)

        # execute the command
        subprocess.check_call(fastqc_command, shell=True)

def main(args):
    """ Make a fastqx object for the Dutch and Brazilian cohort. """

    # make fastqc object for the Ducth cohort
    fastqc_NL = Fastqc('/commons/Themas/Thema06/Themaopdracht/'\
                       'macaque_story/RNA_Microglia/MacaqueNL/fastqFiles',
                       '/data/storage/students/2018-2019/Thema06/groep3/fastqc_NL')

    # get the input files for the NL cohort
    fastq_files_NL = fastqc_NL.get_files()

    # loop through the input files and run the command per file
    for file in fastq_files_NL:
        fastqc_NL.run_command(file)


    # make fastqc object for the Brazilian cohort
    fastqc_BZ = Fastqc('/commons/Themas/Thema06/Themaopdracht/'\
                       'macaque_story/RNA_Microglia/MacaqueBrazil/fastqFiles',
                       '/data/storage/students/2018-2019/Thema06/groep3/fastqc_BZ')

    # get the input files for the BZ cohort
    fastq_files_BZ = fastqc_BZ.get_files()

    # loop through the input files and run the command per file
    for file in fastq_files_BZ:
        fastqc_BZ.run_command(file)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
