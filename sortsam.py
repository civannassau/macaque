#!/usr/bin/env python3

"""
Sort the input BAM files with the
SortSam tool from Picard.
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import sys
import os
import subprocess


class SortSam():
    """
    Class that sorts input files on queryname.
    """

    def __init__(self, picard_file, input_path, output_path):
        """
        Method that gets the attributes that every object will use.
        :param picard_file: path to the picard file used for the tools
        :param input_path: path to the folder with the input files
        :param output_path: path for the output folder
        """
        self.picard_file = picard_file
        self.input_path = input_path
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files
        :return: list with the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)

        # return the files
        return files_list

    def run_command(self, input_file):
        """
        Make the command to sort the given bam files.
        :param input_file: an input sam or bam file that will be sorted
        """
        # the command for SortSam tool
        sortsam_command = 'java -jar {} SortSam I={}/{} O={}/sort_{} SORT_ORDER=queryname'\
                            .format(self.picard_file,
                                    self.input_path, input_file,
                                    self.output_path, input_file.replace('bowtie_', ''))

        # execute the command
        subprocess.check_call(sortsam_command, shell=True)


def main(args):
    """
    Make a sortsam object for the Ducth and Brazilian cohort.
    """

    # object for the NL cohort
    sortsam_NL = SortSam('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                         '/data/storage/students/2018-2019/Thema06/groep3/convert_to_bam_NL',
                         '/data/storage/students/2018-2019/Thema06/groep3/sortsam_NL')

    # get the input files
    input_files_NL = sortsam_NL.get_files()

    # loop through the input files and run the command per file
    for file in input_files_NL:
        sortsam_NL.run_command(file)


    # object for the BZ cohort
    sortsam_BZ = SortSam('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                         '/data/storage/students/2018-2019/Thema06/groep3/convert_to_bam_BZ',
                         '/data/storage/students/2018-2019/Thema06/groep3/sortsam_BZ')

    # get the input files
    input_files_BZ = sortsam_BZ.get_files()

    # loop through the input files and run the command per file
    for file in input_files_BZ:
        sortsam_BZ.run_command(file)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
