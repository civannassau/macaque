#!/usr/bin/env python3

"""
Execute the FixMate tool from
picard on the input files.
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import sys
import os
import subprocess


class FixMate():
    """
    Class that gets the input files and executes
    the FixMate tool on the input files.
    """

    def __init__(self, picard_file, input_path, output_path):
        """
        Method that gets the attributes that every object will use.
        :param picard_file: path to the picard file used for the tools
        :param input_path: path to the input files
        :param output_path: path for the output files
        """
        self.picard_file = picard_file
        self.input_path = input_path
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files
        :return: list with the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)

        # return the files
        return files_list

    def run_command(self, input_file, number_of_threads):
        """
        Make the command to execute FixMate on the input files.
        :param input_file: an input sam or bam file
        """
        # the command for FixMate
        fixmate_command = 'java -XX:ParallelGCThreads={} -jar {} ' \
                          'FixMateInformation I={}/{} O={}/fixmate_{} '\
                            .format(number_of_threads,
                                    self.picard_file,
                                    self.input_path, input_file,
                                    self.output_path, input_file.replace('readgr_', ''))

        # execute the command
        subprocess.check_call(fixmate_command, shell=True)


def main(args):
    """
    Make a FixMate object for the Dutch and Brazilian cohort.
    """
    # object for the NL cohort
    fixmate_NL = FixMate('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                         '/data/storage/students/2018-2019/Thema06/groep3/readgroups_NL',
                         '/data/storage/students/2018-2019/Thema06/groep3/fixmate_NL')

    # get the input files
    input_files_NL = fixmate_NL.get_files()

    # loop through the input files and run the command per file
    for file in input_files_NL:
        fixmate_NL.run_command(file, 20)

    # object for the BZ cohort
    fixmate_BZ = FixMate('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                         '/data/storage/students/2018-2019/Thema06/groep3/readgroups_BZ',
                         '/data/storage/students/2018-2019/Thema06/groep3/fixmate_BZ')

    # get the input files
    input_files_BZ = fixmate_BZ.get_files()

    # loop through the input files and run the command per file
    for file in input_files_BZ:
        fixmate_BZ.run_command(file, 20)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
