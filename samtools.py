#!/usr/bin/env python3

"""
Sort the final bam files
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import sys
import os
import subprocess


class FinalSort():
    """
    Class that gets the files from a given folder
    and a method with an command for samtools sort
    that will be executed for a file.
    """

    def __init__(self, input_path, output_path):
        """
        Method that gets the attributes that every object will use.
        :param input_path: path to the input files
        :param output_path: path for the output files
        """

        self.input_path = input_path
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files
        :return: list with the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)
        files = []
        # only get the bam files
        for file in files_list:
            if file.endswith('.bam'):
                files.append(file)
        return files

    def run_command(self, input_file, number_of_cores):
        """
        Make the command for the samtools sort tool.
        :param input_file: an input sam or bam file
        """
        # the command for samtools sort
        sort_again_command = 'samtools sort -n {}/{} -o {}/sorted_{} -@ {}'\
                             .format(self.input_path, input_file,
                                     self.output_path, input_file.replace('dupl_', ''),
                                     number_of_cores)

        # execute the command and wait until it completes before running the next
        subprocess.check_call(sort_again_command, shell=True)


def main(args):
    """
    Make the objects for executing samtools on the
    Dutch and Brazilian cohort.
    """
    # object for the NL cohort
    sort_NL = FinalSort('/data/storage/students/2018-2019/Thema06/groep3/markduplicates_NL',
                        '/data/storage/students/2018-2019/Thema06/groep3/sortsamtools2_NL')

    # get the input files
    input_files_NL = sort_NL.get_files()

    # loop through the input files and run the command per file
    for file in input_files_NL:
        sort_NL.run_command(file, 40)

    # object for the BZ cohort
    sort_BZ = FinalSort('/data/storage/students/2018-2019/Thema06/groep3/markduplicates_BZ',
                        '/data/storage/students/2018-2019/Thema06/groep3/sortsamtools2_BZ')

    # get the input files
    input_files_BZ = sort_BZ.get_files()

    # loop through the input files and run the command per file
    for file in input_files_BZ:
        sort_BZ.run_command(file, 40)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
