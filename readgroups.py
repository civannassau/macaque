#!/usr/bin/env python3

"""
Add readgroups to the input BAM files.
Readgroups are added with the AddOrReplaceReadGroups
tool from picard.
"""

__author__ = 'Carlijn Ruijter'

# IMPORT
import sys
import os
import subprocess


class ReadGroups():
    """
    Class that gets the files from a given folder
    and executes the command for the readgroup tool.
    """

    def __init__(self, picard_file, input_path, output_path):
        """
        Method that gets the attributes that every object will use.
        :param picard_file: path to the picard file used for the tools
        :param input: path to the input files
        :param output_path: path for the output files
        """
        self.picard_file = picard_file
        self.input_path = input_path
        self.output_path = output_path

    def get_files(self):
        """
        Get a list with the input files
        :return: list with the files
        """

        # use os to get all the files from the input folder
        files_list = os.listdir(self.input_path)

        # return the files
        return files_list

    def run_command(self, input_file):
        """
        Make the command to add readgroups to the given sorted bam files.
        :param input_file: an input sam or bam file
        """

        # the command for ReadGroups
        readgroups_command = 'java -jar {} AddOrReplaceReadGroups I={}/{} O={}/readgr_{} ' \
                             'LB=unknown PU=unknown SM=unknown PL=illumina CREATE_INDEX=true'\
                                .format(self.picard_file,
                                        self.input_path, input_file,
                                        self.output_path, input_file.replace('sort_', ''))

        # execute the command
        subprocess.check_call(readgroups_command, shell=True)


def main(args):
    """
    Make a readgroup object for the Dutch and Brazilian cohort.
    """

    # object for the NL cohort
    readgroups_NL = ReadGroups('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                               '/data/storage/students/2018-2019/Thema06/groep3/sortsam_NL',
                               '/data/storage/students/2018-2019/Thema06/groep3/readgroups_NL')

    # get the input files
    input_files_NL = readgroups_NL.get_files()

    # loop through the input files and run the command per file
    for file in input_files_NL:
        readgroups_NL.run_command(file)


    # object for the BZ cohort
    readgroups_BZ = ReadGroups('/data/storage/students/2018-2019/Thema06/groep3/picard/picard.jar',
                               '/data/storage/students/2018-2019/Thema06/groep3/sortsam_BZ',
                               '/data/storage/students/2018-2019/Thema06/groep3/readgroups_BZ')

    # get the input files
    input_files_BZ = readgroups_BZ.get_files()


    # loop through the input files and run the command per file
    for file in input_files_BZ:
        readgroups_BZ.run_command(file)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
